import Text.Printf
import System.IO

-- getChunk :: Int -> String -> String
-- getChunk 0 subject = ("", subject)
-- getChunk len "" = ""
-- getChunk len subject =
--     let (hd, tl) = (getChunk (len-1) (tail subject))
--     (hd, (head subject) : tl)

chunksOfString :: Int -> String -> [String]

chunksOfString 0 subject = [subject]
chunksOfString len "" = []
chunksOfString len subject = do
    (take len subject):(chunksOfString len (drop len subject))




splitStrOnce :: String -> String -> (String, String)
splitStrOnce separator "" =  ("", "")
splitStrOnce "" subject =  (subject, "")
splitStrOnce separator subject = do
    let sepLen = length separator
    if (take sepLen subject) == separator then
        ("", (drop sepLen subject))
    else do
        let res = splitStrOnce separator (tail subject)
        (((head subject):(fst res)), (snd res))

splitStr :: String -> String -> [String]
splitStr separator "" = []
splitStr separator subject = do
    let res = splitStrOnce separator subject
    (fst res):(splitStr separator (snd res))

intOfString :: String -> Int
intOfString x = read x :: Int

-- addPointToDiagram :: (Int, Int) -> [[Int]]

-- rows -> columns -> diagram
makeDiagram :: Int -> Int -> [[Int]]
makeDiagram rows columns = replicate rows (replicate columns 0)

addLinesToDiagram diagram (line:remaining) = 
    addLinesToDiagram (addToDiagram line) remaining
    where addToDiagram ((xA, yA), (xB, yB)) = 
        undefined


main :: IO ()
main = do
    -- parse input of the day 5 of advent of code 2021
    input <- getContents
    let parsed = map (\(a:[b]) -> (a,b)) $ map (\x -> do {
            map (\(x:[y]) -> ((intOfString x), (intOfString y)))
            $ map (splitStr ",")
            $ splitStr " -> " x
        }) $ lines $ input

    let allPoints = concat $ map (\(a,b) -> [a,b]) $ parsed
    let allXs = map (\(x,y) -> x) $ allPoints
    let allYs = map (\(x,y) -> y) $ allPoints
    let maxX = maximum allXs
    let maxY = maximum allYs

    let diagram = makeDiagram (maxY+1) (maxY+1)

    putStrLn (printf "diagram size: rows: %d; columns: %d" (maxY+1) (maxX+1))

    diagram = map addLineToDiagram (parsed

    putStrLn $ show $ allXs
