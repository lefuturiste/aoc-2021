import fileinput

data = []
for inp in fileinput.input():
    val = inp.strip()
    if val != '':
        int_value = int(val)
        data.append(int_value)
prec = None
count = 0
for i in range(len(data) - 2):
    s = data[i]+data[i+1]+data[i+2]
    if prec != None and s > prec:
        count += 1
    prec = s

print(count)

