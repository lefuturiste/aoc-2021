import fileinput

previous = None
count = 0
for i,line in enumerate(fileinput.input()):
    try:
        val = int(line.strip())
    except ValueError:
        continue
    if previous != None and  val > previous:
        count += 1
    previous = val

print(count)
