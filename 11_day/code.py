import pprint
import fileinput

rows = []
for line in fileinput.input():
	l = line.strip()
	if l == '':
		continue
	rows.append(list(map(int, list(l))))

pprint.pprint(rows)

def print_table(rows):
	for r in rows:
		print(''.join(map(str, r)))

def is_all_zeros(rows):
	return list(map(lambda r: ''.join(map(str, r)), rows)) == 10*[10*'0']

print_table(rows)	

flashes = 0
for step in range(10000):
	print(f'step #{step}')
	locked = []
	pops = 1
	while pops > 0:
		pops = 0
		for i in range(10):
			for j in range(10):
				if rows[i][j] >= 9:
					pops += 1
					flashes += 1
					diagonals = [
						(i-1, j-1),
						(i-1, j),
						(i-1, j+1),
						(i, j-1),
						(i, j+1),
						(i+1, j-1),
						(i+1, j),
						(i+1, j+1)
					]
					rows[i][j] = 0
					locked.append((i, j))
					for y,x in diagonals:
						if not (0 <= y < 10 and 0 <= x < 10): continue
						if (y, x) in locked: continue
						rows[y][x] += 1

	for i in range(10):
		for j in range(10):
			if (i, j) in locked: continue
			rows[i][j] += 1

	if is_all_zeros(rows): 
		print(rows)
		print_table(rows)
		print(step+1)
		break

	print_table(rows)

pprint.pprint(rows)
print(flashes)


