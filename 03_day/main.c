#include <string.h>
#include <stdio.h>

int tobin(char *inp)
{
  int len = strlen(inp);
  printf("%d", len);
  int out = 0;
  int i = 0;
  while (inp[i] != '\0') {
    if (inp[i] == '1') {
      out = out | (1 << len-i-1);
    }
    i++;
  }
  return out;
}

int main(int argc, char** argv) {
  char input;
  char lines[1024][32];
  int lineCount = 0;
  int charCount = 0;
  while (scanf("%c", &input) == 1) {
    if (input == '\n') {
      charCount = 0;
      lineCount++;
    } else {
       lines[lineCount][charCount] = input;
       charCount++;
    }
  }
  
  charCount = strlen(lines[0]);

  char gamma[32];
  char epsilon[32];
  printf("Recorded %d %d \n", lineCount, charCount);
  for (int i = 0; i < charCount; i++) {
    int oneCount = 0;
    for (int j = 0; j < lineCount; j++) {
       printf("%c", lines[j][i]);
       if (lines[j][i] == '1')
         oneCount++;
    }
    printf("\n count: %d \n", oneCount);
    if (oneCount > (lineCount-oneCount)) {
      epsilon[i] = '0';
      gamma[i] = '1';
    } else {
      epsilon[i] = '1';
      gamma[i] = '0';
    }
    printf("\n");
  }
  gamma[i+1] = '\0';
  epsilon[i+1] = '\0';
  printf("gamma: %s  epsilon: %s \n", gamma, epsilon);

  int e = tobin(epsilon);
  int g = tobin(gamma);
  
  printf("%d, %d \n", g, e);
  printf("ans: %d \n", e*g);
}

