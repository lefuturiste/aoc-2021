import fileinput

data = []
for inp in fileinput.input():
    val = inp.strip()
    if val != '':
        cmd, v = val.split()
        data.append((cmd, int(v)))

horizontal = 0
aim = 0
depth = 0
for cmd, val in data:
    if cmd == 'forward':
        horizontal+=val
        depth+=aim*val
    if cmd == 'down':
        aim+=val
    if cmd == 'up':
        aim-=val
print(horizontal, depth)

print(horizontal* depth)
