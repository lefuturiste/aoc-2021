for f in *_day
do
    fullfile="$f"
    filename=$(basename "$fullfile")
    new=$(echo $filename | awk '{split($1,a,"_"); printf "%s",a[1]}')
    len=$(echo -n "$new" | wc -c)
    if [[ $len == 1 ]]; then
        new="0"$filename
    else
        new=$filename
    fi
    mv $filename $new
done
