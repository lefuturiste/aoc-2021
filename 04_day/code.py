import fileinput
import pprint

data = []
for inp in fileinput.input():
    val = inp.strip()
    if val == '':
    	data.append(None)
    if val != '':
    	data.append(val)

# parse called numbers
to_be_called = list(map(int, data[0].strip().split(',')))

# parse boards
boards = []
currentBoard = []
for line in data[2:]:
	if line == None:
		boards.append(currentBoard)
		currentBoard = []
		continue
	line_tmp = filter(lambda x: x != '', line.split(' '))
	line_tmp = map(int, line_tmp)
	currentBoard.append(list(line_tmp))

def is_row_valid(row, called):
	to_cmp = list(map(lambda x: x in called, row))
	return to_cmp == len(row)*[True]

def get_wins():
	wins = []
	for i in range(len(to_be_called)):
		calling = to_be_called[0:(i+1)]
		for board_index,board in enumerate(boards):
			if len(list(filter(lambda x: x[0] == board_index, wins))) > 0:
				continue
			for row in board:
				if is_row_valid(row, calling):
					wins.append(board_index, calling, board)
			# generate columns
			columns = []
			for column_index in range(len(board[0])):
				current_c = []
				for row_index in range(len(board)):
					current_c.append(board[row_index][column_index])

			for column in columns:
				if is_row_valid(column, calling):
					wins.append(board_index, calling, board)

	return False

def get_score_from_board(board, calling):
	sum_unmarked = 0
	for row in board:
		for e in row:
			if e not in calling:
				sum_unmarked += e
	return sum_unmarked * calling[-1]

score = 0
wins = get_wins()
pprint.pprint(wins)
calling, winner = wins[0]
if winner:
	score = get_score_from_board(winner, calling)

pprint.pprint(winner)
print(f'{score=}')