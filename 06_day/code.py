
db = False

import fileinput

state = []

lines = [l for l in fileinput.input()]
assert(len(lines) > 0)
assert(len(lines[0]) > 0)

rawState = list(map(int, lines[0].split(',')))

state = [0 for i in range(9)]

for s in rawState:
	state[s] += 1

days = 256
day = 0

if db:
	print(day, ",".join(map(str, state)))

for day in range(1, days+1):
	nextState = [0 for i in range(9)]
	for i, s in enumerate(state):
		if i == 0:
			nextState[6] = s
			nextState[8] = s
		else:
			nextState[i-1] += s

	state = nextState.copy()

	if db:
		print(day, ",".join(map(str, state)))

print('===')
print(sum(state))
